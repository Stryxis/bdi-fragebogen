package Questionaire;

public class Auswertung {

   private String[] ergebnisse;
   private String interpretation;
   private int punktzahl;


    public Auswertung(String[] ergebnisse, int punktzahl){

        this.ergebnisse = ergebnisse;
        this.punktzahl = punktzahl;
    }

    private String createInterpretation() {
        String back;
        if(punktzahl <= 8){
            back = "\nEs liegt eine KEINE Depression vor";
        } else {
            if (punktzahl >= 9 && punktzahl <= 13){
                back = "\nEs liegt eine MINIMALE Depression vor";
            } else {
                if(punktzahl >= 14 && punktzahl <= 19) {
                    back = "\nEs liegt eine LEICHTE Depression vor";
                } else {
                    if(punktzahl >= 20 && punktzahl <= 28) {
                        back = "\nEs liegt eine MITTELSCHWERE Depression vor";
                    } else {
                        back = "\nEs liegt eine SCHWERE Depression vor";

                    }
                }
            }
        }

        return back;
    }

    private String createErgebnisText(){

        String back = "";

        for (int i = 0; i < ergebnisse.length; i++) {
            back = back + ergebnisse[i] + "\n";
        }

        return back;
    }

    private String createPunktzahlText(){
        String back;

        back = "\nDas Ergebnis des Fragebogens ist: " + punktzahl;

        return back;
    }

    public String createText(){
        return createErgebnisText() + createPunktzahlText() + createInterpretation();
    }
}

