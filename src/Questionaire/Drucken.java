package Questionaire;

import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

public class Drucken implements Printable {

    private String ergebnis;
    private int punkte;

    public Drucken(String ergebnis, int punkte) {
        this.ergebnis = ergebnis;
        this.punkte = punkte;
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        int tmp = 100;
        if(pageIndex > 0) {
            return NO_SUCH_PAGE;
        }
        Graphics2D g2d = (Graphics2D)graphics;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        g2d.drawString(ergebnis,40,40);
        return PAGE_EXISTS;
    }
}
