package Questionaire;

public class SevenItem extends Item {

    private String title,a1,a2,a3,a4,a5,a6,a7;

    SevenItem (String title, String one, String two, String three, String four, String five, String six, String seven) {
        this.title = title;
        a1 = one;
        a2 = two;
        a3 = three;
        a4 = four;
        a5 = five;
        a6 = six;
        a7 = seven;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }

    public void setA4(String a4) {
        this.a4 = a4;
    }

    public void setA5(String a5) {
        this.a5 = a5;
    }

    public void setA6(String a6) {
        this.a6 = a6;
    }

    public void setA7(String a7) {
        this.a7 = a7;
    }

    public String getA1() {
        return a1;
    }

    public String getA2() {
        return a2;
    }

    public String getA3() {
        return a3;
    }

    public String getA4() {
        return a4;
    }

    public String getA5() {
        return a5;
    }

    public String getA6() {
        return a6;
    }

    public String getA7() {
        return a7;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
