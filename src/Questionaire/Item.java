package Questionaire;

public abstract class Item {

    Item() {
    }

    public abstract String getA1();
    public abstract String getA2();
    public abstract String getA3();
    public abstract String getA4();
    public abstract String getA5();
    public abstract String getA6();
    public abstract String getA7();
    public abstract String getTitle();


}
