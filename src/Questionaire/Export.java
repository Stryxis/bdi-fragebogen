package Questionaire;

import UI.Chiffre;

import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Export {

    private int punktzahl;
    private FileWriter fw;
    private BufferedWriter bw;
    private Timestamp timestamp;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy'_'HH.mm");

    public Export(String[] input, int punkte) throws IOException {

        punktzahl = punkte;

        timestamp = new Timestamp(System.currentTimeMillis());
        fw = new FileWriter(Chiffre.name + Chiffre.datum + ".txt");
        bw = new BufferedWriter(fw);
        for (int i = 0; i < input.length; i++) {
            bw.write(input[i]);
            bw.newLine();
        }
        bw.newLine();
        bw.write("Das Ergebnis des Fragebogens ist: " + punkte);
        bw.newLine();
        bw.write(createInterpretation());
        bw.close();
        fw.close();
    }

    private String createInterpretation() {
        String back;
        if(punktzahl <= 8){
            back = "\nEs liegt eine KEINE Depression vor";
        } else {
            if (punktzahl >= 9 && punktzahl <= 13){
                back = "\nEs liegt eine MINIMALE Depression vor";
            } else {
                if(punktzahl >= 14 && punktzahl <= 19) {
                    back = "\nEs liegt eine LEICHTE Depression vor";
                } else {
                    if(punktzahl >= 20 && punktzahl <= 28) {
                        back = "\nEs liegt eine MITTELSCHWERE Depression vor";
                    } else {
                        back = "\nEs liegt eine SCHWERE Depression vor";

                    }
                }
            }
        }

        return back;
    }
}
