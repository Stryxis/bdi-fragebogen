package Questionaire;

import UI.Chiffre;
import UI.Deckblatt;

import javax.swing.*;
import java.awt.*;

public class launch {

    private static JFrame frame;
    private static Bogen bogen;
    private static String[] ergebnisse;

    public static void main(String[] args) {
        bogen = new Bogen();
        ergebnisse = new String[21];
        EventQueue.invokeLater(() -> {
            try {
                frame = new JFrame();
                frame.setVisible(true);
                Chiffre chiffre = new Chiffre(frame, bogen, ergebnisse);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
