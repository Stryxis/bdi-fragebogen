package Questionaire;

public class OneItem extends Item {

    private String title,a1;

    OneItem(String title, String one) {
        this.title = title;
        a1 = one;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA1() {
        return a1;
    }

    @Override
    public String getA2() {
        return null;
    }

    @Override
    public String getA3() {
        return null;
    }

    @Override
    public String getA4() {
        return null;
    }

    @Override
    public String getA5() {
        return null;
    }

    @Override
    public String getA6() {
        return null;
    }

    @Override
    public String getA7() {
        return null;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return getA1();
    }
}
