package Questionaire;

public class FourItem extends Item {

    private String title,a1,a2,a3,a4;

    FourItem(String title, String one, String two, String three, String four) {

        this.title = title;
        a1 = one;
        a2 = two;
        a3 = three;
        a4 = four;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }

    public void setA4(String a4) {
        this.a4 = a4;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getA1() {
        return a1;
    }

    public String getA2() {
        return a2;
    }

    public String getA3() {
        return a3;
    }

    public String getA4() {
        return a4;
    }

    @Override
    public String getA5() {
        return null;
    }

    @Override
    public String getA6() {
        return null;
    }

    @Override
    public String getA7() {
        return null;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
