package Questionaire;


public class Bogen {

    private Item[] fragen;

    /**
     *
     */
    Bogen() {
        fragen = new Item[23];
        fillArray();
    }

    private Item initDeckblatt() {
        Item item = new OneItem("Startbildschirm","Dieser Fragebogen enthält 21 Gruppen von Aussagen. " +
                "Bitte lesen Sie jede dieser Gruppen von Aussagen sorgfältig durch und suchen Sie sich dann in jeder Gruppe eine Aussage heraus, die am besten beschreibt, " +
                "wie Sie sich in den letzten zwei Wochen, einschließlich heute, gefühlt haben. " +
                "Kreuzen Sie die Zahl neben der Aussagen an, die Sie sich herausgesucht haben (0, 1 ,2 oder 3). " +
                "Falls in einer Gruppe mehrere Aussagen gleichermaßen auf Sie zutreffen, kreuzen Sie die Aussage mit der höheren Zahl an. " +
                "Achten Sie bitte darauf, dass Sie in jeder Gruppe nicht mehr als eine Aussage ankreuzen, das gilt auch für Gruppe 16 (Veränderung der Schlafgewohnheiten) oder " +
                "Gruppe 18 (Veränderung des Appetits).");
        return item;
    }

    private Item initItem1() {
        String title = "Traurigkeit";
        String a1 = "0 - Ich bin nicht traurig";
        String a2 = "1 - Ich bin oft traurig";
        String a3 = "2 - Ich bin ständig traurig";
        String a4 = "3 - Ich bin so traurig oder unglücklich, dass ich es nicht aushalte";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem2() {
        String title = "Pessimismus";
        String a1 = "0 - Ich sehe nicht mutlos in die Zukunft";
        String a2 = "1 - Ich sehe mutloser in die Zukunft als sonst";
        String a3 = "2 - Ich bin mutlos und erwarte nicht, dass meine Situation besser wird";
        String a4 = "3 - Ich glaube, dass meine Zukunft hoffnungslos ist und nur noch schlechter wird";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem3() {
        String title = "Versagensgefühle";
        String a1 = "0 - Ich fühle mich nicht als Versager";
        String a2 = "1 - Ich habe häufiger Versagensgefühle";
        String a3 = "2 - Wenn ich zurückblicke, sehe ich eine Menge Fehlschläge";
        String a4 = "3 - Ich habe das Gefühl, als Mensch ein völliger Versager zu sein";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem4() {
        String title = "Verlust von Freude";
        String a1 = "0 - Ich kann die Dinge genauso gut genießen wie früher";
        String a2 = "1 - Ich kann die Dinge nicht mehr so genießen wie früher";
        String a3 = "2 - Dinge, die mir früher Freude gemacht haben, kann ich kaum mehr genießen";
        String a4 = "3 - Dinge, die mir früher Freude gemacht haben, kann ich überhaupt nicht mehr genießen";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem5() {
        String title = "Schuldgefühle";
        String a1 = "0 - Ich habe keine besonderen Schuldgefühle";
        String a2 = "1 - Ich habe oft Schuldgefühle wegen Dingen, die ich getan habe oder hätte tun sollen";
        String a3 = "2 - Ich hab die meiste Zeit Schuldgefühle";
        String a4 = "3 - Ich habe ständig Schuldgefühle";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem6() {
        String title = "Bestrafungsgefühle";
        String a1 = "0 - Ich habe nicht das Gefühl, für etwas bestraft zu sein";
        String a2 = "1 - Ich hab das Gefühl, vielleicht bestraft zu werden";
        String a3 = "2 - Ich erwarte, bestraft zu werden";
        String a4 = "3 - Ich habe das Gefühl, bestraft zu sein";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem7() {
        String title = "Selbstablehnung";
        String a1 = "0 - Ich halte von mir genauso viel wie immer";
        String a2 = "1 - Ich habe Vertrauen in mich verloren";
        String a3 = "2 - Ich bin von mir enttäuscht";
        String a4 = "3 - Ich lehne mich völlig ab";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem8() {
        String title = "Selbstvorwürfe";
        String a1 = "0 - Ich kritisiere oder tadle mich nicht mehr als sonst";
        String a2 = "1 - Ich bin mir gegenüber kritischer als sonst";
        String a3 = "2 - Ich kritisiere mich für all meine Mängel";
        String a4 = "3 - Ich gebe mir die Schuld für alles Schlimme, was passiert";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem9() {
        String title = "Selbstmordgedanken";
        String a1 = "0 - Ich denke nicht daran, mir etwas anzutun";
        String a2 = "1 - Ich denke manchmal an Selbstmord, aber ich würde es nicht tun";
        String a3 = "2 - Ich möchte mich am liebsten umbringen";
        String a4 = "3 - Ich würde mich umbringen, wenn ich die Gelegenheit dazu hätte";
        final Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem10() {
        String title = "Weinen";
        String a1 = "0 - Ich weine nicht öfter als früher";
        String a2 = "1 - Ich weine jetzt mehr als früher";
        String a3 = "2 - Ich weine beim geringsten Anlass";
        String a4 = "3 - Ich möchte gerne weinen, aber ich kann nicht";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem11() {
        String title = "Unruhe";
        String a1 = "0 - Ich bin nicht unruhiger als sonst";
        String a2 = "1 - Ich bin unruhiger als sonst";
        String a3 = "2 - Ich bin so unruhig, dass es mir schwer fällt, stillzusitzen";
        String a4 = "3 - Ich bin so unruhig, dass ich mich ständig bewegen oder etwas tun muss";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem12() {
        String title = "Interessensverlust";
        String a1 = "0 - Ich habe das Interesse an anderen Menschen oder an Tätigkeiten nicht verloren";
        String a2 = "1 - Ich habe weniger Interesse an anderen Menschen oder an Dingen als sonst";
        String a3 = "2 - Ich habe das Interesse an anderen Menschen oder an Dingen zum größten Teil verloren";
        String a4 = "3 - Es fällt mir schwer, mich überhaupt für irgend etwas zu interessieren";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem13() {
        String title = "Entschlussunfähigkeit";
        String a1 = "0 - Ich bin so entschlussfreudig wie immer";
        String a2 = "1 - Es fällt mir schwerer als sonst, Entscheidungen zu treffen";
        String a3 = "2 - Es fällt mir sehr viel schwerer als sonst, Entscheidungen zu treffen";
        String a4 = "3 - Ich habe Mühe, überhaupt Entscheidungen zu treffen";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem14() {
        String title = "Wertlosigkeit";
        String a1 = "0 - Ich fühle mich nicht wertlos";
        String a2 = "1 - Ich halte mich für weniger wertvoll und nützlich als sonst";
        String a3 = "2 - Verglichen mit anderen Menschen fühle ich mich viel weniger wert";
        String a4 = "3 - Ich fühle mich völlig wertlos";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem15() {
        String title = "Energieverlust";
        String a1 = "0 - Ich habe so viel Energie wie immer";
        String a2 = "1 - Ich habe weniger Energie als sonst";
        String a3 = "2 - Ich habe so wenig Energie, dass ich kaum noch etwas schaffe";
        String a4 = "3 - Ich habe keine Energie mehr, um überhaupt noch etwas zu tun";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem16() {
        String title = "Veränderung der Schlafgewohnheiten";
        String a1 = "0 - Meine Schlafgewohnheiten haben sich nicht verändert";
        String a2 = "1a - Ich schlafe etwas mehr als sonst";
        String a3 = "1b - Ich schlafe etwas weniger als sonst";
        String a4 = "2a - Ich schlafe viel mehr als sonst";
        String a5 = "2b - Ich schlafe viel weniger als sonst";
        String a6 = "3a - Ich schlafe fast den ganzen Tag";
        String a7 = "3b - Ich wache 1-2 Stunden früher auf als gewöhnlich und kann nicht mehr einschlafen";
        Item item = new SevenItem(title, a1, a2, a3, a4, a5, a6, a7);
        return item;
    }

    private Item initItem17() {
        String title = "Reizbarkeit";
        String a1 = "0 - Ich bin nicht reizbarer als sonst";
        String a2 = "1 - Ich bin reizbarer als sonst";
        String a3 = "2 - Ich bin viel reizbarer als sonst";
        String a4 = "3 - Ich fühle mich dauernd gereizt";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem18() {
        String title = "Veränderung des Appetits";
        String a1 = "0 - Mein Appetit hat sich nicht verändert";
        String a2 = "1a - Mein Appetit ist etwas schlechter als sonst";
        String a3 = "1b - Mein Appetit ist etwas größer als sonst";
        String a4 = "2a - Mein Appetit ist viel schlechter als sonst";
        String a5 = "2b - Mein Appetit ist viel größer als sonst";
        String a6 = "3a - Ich habe überhaupt keinen Appetit";
        String a7 = "3b - Ich habe ständig Heißhunger";
        Item item = new SevenItem(title, a1, a2, a3, a4, a5, a6, a7);
        return item;
    }

    private Item initItem19() {
        String title = "Konzentrationsschwierigkeiten";
        String a1 = "0 - Ich kann mich so gut konzentrieren wie immer";
        String a2 = "1 - Ich kann mich nicht mehr so gut konzentrieren wie sonst";
        String a3 = "2 - Es fällt mir schwer, mich längere Zeit auf irgend etwas zu konzentrieren";
        String a4 = "3 - Ich kann mich überhaupt nicht mehr konzentrieren";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem20() {
        String title = "Ermüdug oder Erschöpfung";
        String a1 = "0 - Ich fühle mich nicht müder oder erschöpfter als sonst";
        String a2 = "1 - Ich werde schneller müder oder erschöpfter als sonst";
        String a3 = "2 - Für viele Dinge, die ich üblicherweise tue, bin ich zu müde oder erschöpft";
        String a4 = "3 - Ich bin so müde oder erschöpft, dass ich fast nichts mehr tun kann";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem21() {
        String title = "Verlust an sexuellem Interesse";
        String a1 = "0 - Mein Interesse an Sexualität hat sich in letzter Zeit nicht verändert";
        String a2 = "1 - Ich interessiere mich weniger für Sexualität als früher";
        String a3 = "2 - Ich interessiere mich jetzt viel weniger für Sexualität";
        String a4 = "3 - Ich habe das Interesse an Sexualität völlig verloren";
        Item item = new FourItem(title, a1, a2, a3, a4);
        return item;
    }

    private Item initItem22() {
        String title = "Ende";
        String a1 = "Vielen Dank für das Ausfüllen des Fragebogens.";
        Item item = new OneItem(title, a1);
        return item;
    }

    private void fillArray() {
        fragen[0] = initDeckblatt();
        fragen[1] = initItem1();
        fragen[2] = initItem2();
        fragen[3] = initItem3();
        fragen[4] = initItem4();
        fragen[5] = initItem5();
        fragen[6] = initItem6();
        fragen[7] = initItem7();
        fragen[8] = initItem8();
        fragen[9] = initItem9();
        fragen[10] = initItem10();
        fragen[11] = initItem11();
        fragen[12] = initItem12();
        fragen[13] = initItem13();
        fragen[14] = initItem14();
        fragen[15] = initItem15();
        fragen[16] = initItem16();
        fragen[17] = initItem17();
        fragen[18] = initItem18();
        fragen[19] = initItem19();
        fragen[20] = initItem20();
        fragen[21] = initItem21();
        fragen[22] = initItem22();
    }

    public Item[] getFragen() {
        return fragen;
    }

    public Item getItemAt(int index) {
        return fragen[index];
    }
}
