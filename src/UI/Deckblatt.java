package UI;

import Questionaire.Bogen;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;

public class Deckblatt {

    private static JFrame frame;
    private static Bogen bogen;
    private static String[] ergebnisse;
    private int index;
    private JPanel contentPane;
    private JButton btn_start;
    private Font font;

    public Deckblatt(JFrame frame, Bogen bogen, int index, String[] ergebnisse) {

        font = new Font("Calibri", Font.PLAIN, 22);

        this.index = index;
        Deckblatt.frame = frame;
        Deckblatt.bogen = bogen;
        Deckblatt.ergebnisse = ergebnisse;

        initWindow();
        addAction();
    }

    private void addAction() {
        btn_start.addActionListener(arg0 -> {
            contentPane.setVisible(false);

            new FourItem(frame, bogen, index, ergebnisse);
        });
    }

    private void initWindow() {
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 800, 500);
        frame.setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        frame.setContentPane(contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{424, 0};
        gbl_contentPane.rowHeights = new int[]{318, 33, 0};
        gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl_contentPane);
        frame.setTitle(index + " " + bogen.getItemAt(index).getTitle());

        JTextPane textPane = new JTextPane();
        textPane.setText((bogen.getItemAt(0).getA1()));
        textPane.setBackground(SystemColor.menu);
        textPane.setEditable(false);
        textPane.setFont(new Font("Calibri", Font.PLAIN, 18));
        GridBagConstraints gbc_txtpnHierKnnteIhre = new GridBagConstraints();
        gbc_txtpnHierKnnteIhre.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtpnHierKnnteIhre.insets = new Insets(0, 0, 5, 0);
        gbc_txtpnHierKnnteIhre.gridx = 0;
        gbc_txtpnHierKnnteIhre.gridy = 0;
        contentPane.add(textPane, gbc_txtpnHierKnnteIhre);
        centerText(textPane);

        JPanel panel_bottom = new JPanel();
        GridBagConstraints gbc_panel_bottom = new GridBagConstraints();
        gbc_panel_bottom.anchor = GridBagConstraints.NORTH;
        gbc_panel_bottom.fill = GridBagConstraints.HORIZONTAL;
        gbc_panel_bottom.gridx = 0;
        gbc_panel_bottom.gridy = 1;
        contentPane.add(panel_bottom, gbc_panel_bottom);
        panel_bottom.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        btn_start = new JButton("Start");
        panel_bottom.add(btn_start);

        contentPane.setVisible(true);

        textPane.setFont(font);
    }

    private void centerText(JTextPane textPane) {
        StyledDocument doc = textPane.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_JUSTIFIED);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }
}
