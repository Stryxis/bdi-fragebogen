package UI;

import Questionaire.Bogen;


import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 */
class SevenItem {

    /*

     */
    private static JFrame frame;
    private static Bogen bogen;
    private static String[] ergebnisse;
    private int index;
    private JPanel itemPane;
    private JRadioButton rdbtn_first, rdbtn_second, rdbtn_third, rdbtn_fourth, rdbtn_fifth, rdbtn_sixth, rdbtn_seventh;
    private JTextPane txtpn_first, txtpn_second, txtpn_third, txtpn_fourth, txtpn_fifth, txtpn_sixth, txtpn_seventh;
    private JButton btn_next;
    private Font font;

    /**
     *
     * @param frame
     * @param bogen
     * @param index
     * @param ergebnisse
     */
    SevenItem(JFrame frame, Bogen bogen, int index, String[] ergebnisse) {

        font = new Font("Calibri", Font.PLAIN, 22);
        SevenItem.frame = frame;
        SevenItem.bogen = bogen;
        SevenItem.ergebnisse = ergebnisse;
        this.index = index+1;
        initItemPanel();

    }

    /**
     *
     */
    private void initItemPanel() {

        //Übergebener Frame wird neu eingerichtet und gefüllt

        itemPane = new JPanel();
        itemPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        frame.setContentPane(itemPane);
        frame.setTitle(index + " " + bogen.getItemAt(index).getTitle());
        GridBagLayout gbl_itemPane = new GridBagLayout();
        gbl_itemPane.columnWidths = new int[]{0, 0, 0, 0, 0};
        gbl_itemPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_itemPane.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        gbl_itemPane.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
        itemPane.setLayout(gbl_itemPane);

        JPanel space_top = new JPanel();
        GridBagConstraints gbc_space_top = new GridBagConstraints();
        gbc_space_top.gridwidth = 4;
        gbc_space_top.insets = new Insets(0, 0, 5, 0);
        gbc_space_top.fill = GridBagConstraints.BOTH;
        gbc_space_top.gridx = 0;
        gbc_space_top.gridy = 0;
        itemPane.add(space_top, gbc_space_top);

        JPanel space_left = new JPanel();
        GridBagConstraints gbc_space_left = new GridBagConstraints();
        gbc_space_left.gridheight = 7;
        gbc_space_left.insets = new Insets(0, 0, 5, 5);
        gbc_space_left.fill = GridBagConstraints.BOTH;
        gbc_space_left.gridx = 0;
        gbc_space_left.gridy = 1;
        itemPane.add(space_left, gbc_space_left);

        rdbtn_first = new JRadioButton("");
        initRButton(rdbtn_first,1);

        txtpn_first = new JTextPane();
        initTPane(txtpn_first,1);

        JPanel space_right = new JPanel();
        GridBagConstraints gbc_space_right = new GridBagConstraints();
        gbc_space_right.insets = new Insets(0, 0, 5, 0);
        gbc_space_right.gridheight = 7;
        gbc_space_right.fill = GridBagConstraints.BOTH;
        gbc_space_right.gridx = 3;
        gbc_space_right.gridy = 1;
        itemPane.add(space_right, gbc_space_right);

        rdbtn_second = new JRadioButton("");
        initRButton(rdbtn_second,2);

        txtpn_second = new JTextPane();
        initTPane(txtpn_second,2);

        rdbtn_third = new JRadioButton("");
        initRButton(rdbtn_third,3);

        txtpn_third = new JTextPane();
        initTPane(txtpn_third,3);

        rdbtn_fourth = new JRadioButton("");
        initRButton(rdbtn_fourth,4);

        txtpn_fourth = new JTextPane();
        initTPane(txtpn_fourth,4);

        rdbtn_fifth = new JRadioButton("");
        initRButton(rdbtn_fifth,5);

        txtpn_fifth = new JTextPane();
        initTPane(txtpn_fifth,5);

        rdbtn_sixth = new JRadioButton("");
        initRButton(rdbtn_sixth,6);

        txtpn_sixth = new JTextPane();
        initTPane(txtpn_sixth,6);

        rdbtn_seventh = new JRadioButton("");
        initRButton(rdbtn_seventh,7);

        txtpn_seventh = new JTextPane();
        initTPane(txtpn_seventh,7);

        JPanel space_bottom = new JPanel();
        GridBagConstraints gbc_space_bottom = new GridBagConstraints();
        gbc_space_bottom.gridwidth = 4;
        gbc_space_bottom.anchor = GridBagConstraints.NORTH;
        gbc_space_bottom.insets = new Insets(0, 0, 5, 0);
        gbc_space_bottom.fill = GridBagConstraints.HORIZONTAL;
        gbc_space_bottom.gridx = 0;
        gbc_space_bottom.gridy = 8;
        itemPane.add(space_bottom, gbc_space_bottom);

        btn_next = new JButton("Weiter");
        btn_next.setEnabled(false);

        GridBagConstraints gbc_btn_next = new GridBagConstraints();
        gbc_btn_next.gridwidth = 3;
        gbc_btn_next.insets = new Insets(0, 0, 0, 5);
        gbc_btn_next.gridx = 0;
        gbc_btn_next.gridy = 9;
        itemPane.add(btn_next, gbc_btn_next);

        //Font anwenden
        txtpn_first.setFont(font);
        txtpn_second.setFont(font);
        txtpn_third.setFont(font);
        txtpn_fourth.setFont(font);
        txtpn_fifth.setFont(font);
        txtpn_sixth.setFont(font);
        txtpn_seventh.setFont(font);

        addAction();

    }

    /**
     *
     */
    private void addAction() {

        /*

         */
        rdbtn_first.addActionListener(arg0 -> {
            selektion(1);
        });

        rdbtn_second.addActionListener(arg0 -> {
            selektion(2);
        });

        rdbtn_third.addActionListener(arg0 -> {
            selektion(3);
        });

        rdbtn_fourth.addActionListener(arg0 -> {
            selektion(4);
        });

        rdbtn_fifth.addActionListener(arg0 -> {
            selektion(5);
        });

        rdbtn_sixth.addActionListener(arg0 -> {
            selektion(6);
        });

        rdbtn_seventh.addActionListener(arg0 -> {
            selektion(7);
        });

        rdbtn_first.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER) {
                    weiter();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        rdbtn_second.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER) {
                    weiter();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        rdbtn_third.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER) {
                    weiter();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        rdbtn_fourth.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER) {
                    weiter();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        rdbtn_fifth.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER) {
                    weiter();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        rdbtn_sixth.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER) {
                    weiter();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        rdbtn_seventh.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER) {
                    weiter();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        txtpn_first.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(1);
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });

        txtpn_second.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(2);
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });

        txtpn_third.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(3);
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });

        txtpn_fourth.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(4);
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });

        txtpn_fifth.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(5);
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });

        txtpn_sixth.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(6);
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });

        txtpn_seventh.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(7);
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });
        /*

         */
        btn_next.addActionListener(arg0 -> {
            weiter();
        });
    }

    private void initRButton(JRadioButton jrb, int nummer) {
        GridBagConstraints gbc_rdbtn = new GridBagConstraints();
        gbc_rdbtn.insets = new Insets(0, 0, 5, 5);
        gbc_rdbtn.gridx = 1;
        gbc_rdbtn.gridy = nummer;
        itemPane.add(jrb, gbc_rdbtn);
    }

    private void initTPane(JTextPane jtp, int nummer) {
        jtp.setBackground(SystemColor.menu);

        if (nummer == 1) {
            jtp.setText(bogen.getItemAt(index).getA1());
        } else {
            if (nummer == 2) {
                jtp.setText(bogen.getItemAt(index).getA2());
            } else {
                if(nummer == 3) {
                    jtp.setText(bogen.getItemAt(index).getA3());
                } else {
                    if(nummer == 4) {
                        jtp.setText(bogen.getItemAt(index).getA4());
                    } else {
                        if(nummer == 5) {
                            jtp.setText(bogen.getItemAt(index).getA5());
                        } else {
                            if(nummer == 6) {
                                jtp.setText(bogen.getItemAt(index).getA6());
                            } else {
                                jtp.setText(bogen.getItemAt(index).getA7());
                            }
                        }
                    }
                }
            }
        }

        GridBagConstraints gbc_txtpn = new GridBagConstraints();
        gbc_txtpn.insets = new Insets(0, 0, 5, 5);
        gbc_txtpn.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtpn.gridx = 2;
        gbc_txtpn.gridy = nummer;
        centerText(jtp);
        itemPane.add(jtp, gbc_txtpn);
    }

    private void selektion(int nummer) {
        if(nummer == 1) {
            if(onlyOne()) {
                rdbtn_first.setSelected(true);
            } else {
                rdbtn_first.setSelected(true);
                rdbtn_second.setSelected(false);
                rdbtn_third.setSelected(false);
                rdbtn_fourth.setSelected(false);
                rdbtn_fifth.setSelected(false);
                rdbtn_sixth.setSelected(false);
                rdbtn_seventh.setSelected(false);
            }
        } else {
            if(nummer == 2) {
                if(onlyOne()) {
                    rdbtn_second.setSelected(true);
                } else {
                    rdbtn_first.setSelected(false);
                    rdbtn_second.setSelected(true);
                    rdbtn_third.setSelected(false);
                    rdbtn_fourth.setSelected(false);
                    rdbtn_fifth.setSelected(false);
                    rdbtn_sixth.setSelected(false);
                    rdbtn_seventh.setSelected(false);
                }
            } else {
                if(nummer == 3) {
                    if(onlyOne()) {
                        rdbtn_third.setSelected(true);
                    } else {
                        rdbtn_first.setSelected(false);
                        rdbtn_second.setSelected(false);
                        rdbtn_third.setSelected(true);
                        rdbtn_fourth.setSelected(false);
                        rdbtn_fifth.setSelected(false);
                        rdbtn_sixth.setSelected(false);
                        rdbtn_seventh.setSelected(false);
                    }
                } else {
                    if(nummer == 4) {
                        if(onlyOne()) {
                            rdbtn_fourth.setSelected(true);
                        } else {
                            rdbtn_first.setSelected(false);
                            rdbtn_second.setSelected(false);
                            rdbtn_third.setSelected(false);
                            rdbtn_fourth.setSelected(true);
                            rdbtn_fifth.setSelected(false);
                            rdbtn_sixth.setSelected(false);
                            rdbtn_seventh.setSelected(false);
                        }
                    } else {
                        if(nummer == 5) {
                            if(onlyOne()) {
                                rdbtn_fifth.setSelected(true);
                            } else {
                                rdbtn_first.setSelected(false);
                                rdbtn_second.setSelected(false);
                                rdbtn_third.setSelected(false);
                                rdbtn_fourth.setSelected(false);
                                rdbtn_fifth.setSelected(true);
                                rdbtn_sixth.setSelected(false);
                                rdbtn_seventh.setSelected(false);
                            }
                        } else {
                            if(nummer == 6) {
                                if(onlyOne()) {
                                    rdbtn_sixth.setSelected(true);
                                } else {
                                    rdbtn_first.setSelected(false);
                                    rdbtn_second.setSelected(false);
                                    rdbtn_third.setSelected(false);
                                    rdbtn_fourth.setSelected(false);
                                    rdbtn_fifth.setSelected(false);
                                    rdbtn_sixth.setSelected(true);
                                    rdbtn_seventh.setSelected(false);
                                }
                            } else {
                                if(onlyOne()) {
                                    rdbtn_seventh.setSelected(true);
                                } else {
                                    rdbtn_first.setSelected(false);
                                    rdbtn_second.setSelected(false);
                                    rdbtn_third.setSelected(false);
                                    rdbtn_fourth.setSelected(false);
                                    rdbtn_fifth.setSelected(false);
                                    rdbtn_sixth.setSelected(false);
                                    rdbtn_seventh.setSelected(true);
                                }
                            }
                        }
                    }
                }
            }
        }
        btn_next.setEnabled(true);
        btn_next.grabFocus();
    }

    private void weiter(){
        if (rdbtn_first.isSelected()) {
            ergebnisse[index-1] = bogen.getItemAt(index).getA1();
        } else {
            if(rdbtn_second.isSelected()) {
                ergebnisse[index-1] = bogen.getItemAt(index).getA2();
            } else {
                if(rdbtn_third.isSelected()) {
                    ergebnisse[index-1] = bogen.getItemAt(index).getA3();
                } else {
                    if(rdbtn_fourth.isSelected()) {
                        ergebnisse[index - 1] = bogen.getItemAt(index).getA4();
                    } else {
                        if(rdbtn_fifth.isSelected()) {
                            ergebnisse[index - 1] = bogen.getItemAt(index).getA5();
                        } else {
                            if(rdbtn_sixth.isSelected()) {
                                ergebnisse[index - 1] = bogen.getItemAt(index).getA6();
                            } else {
                                ergebnisse[index - 1] = bogen.getItemAt(index).getA7();
                            }
                        }
                    }
                }
            }
        }
        itemPane.setVisible(false);
        new FourItem(frame, bogen, index, ergebnisse);
    }

    /**
     *
     * @return
     */
    private boolean onlyOne() {
        return !rdbtn_first.isSelected() && !rdbtn_second.isSelected() && !rdbtn_third.isSelected() && !rdbtn_fourth.isSelected() && !rdbtn_fifth.isSelected() && !rdbtn_sixth.isSelected() && !rdbtn_seventh.isSelected();
    }

    private void centerText(JTextPane textPane) {
        StyledDocument doc = textPane.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_LEFT);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }


}
