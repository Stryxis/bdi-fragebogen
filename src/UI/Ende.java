package UI;

import Questionaire.Auswertung;
import Questionaire.Bogen;
import Questionaire.Drucken;
import Questionaire.Export;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.IOException;

class Ende {
    private static JFrame frame;
    private static Bogen bogen;
    private static String[] ergebnisse;
    private JMenuBar menuBar;
    private JMenu mnDatei;
    private JMenuItem mntmExport,mntmErgebnis;
    private int index;
    private JPanel contentPane;
    private JPanel panel_bottom;
    private JTextPane textPane;
    private JScrollPane scrollPane;
    private Auswertung auswertung;
    private Font font;

    Ende(JFrame frame, Bogen bogen, int index, String[] ergebnisse) {

        font = new Font("Calibri", Font.PLAIN, 22);
        Ende.ergebnisse = ergebnisse;
        this.index = index+1;
        Ende.frame = frame;
        Ende.bogen = bogen;
        auswertung = new Auswertung(ergebnisse, getPunktzahl());

        initWindow();
        addAction();
    }

    private void addAction() {
        mntmErgebnis.addActionListener(arg0 -> {

            contentPane.setVisible(false);
            frame.setBounds(100,100,800,700);
            frame.setLocationRelativeTo(null);
            frame.remove(panel_bottom);

            textPane.setText(auswertung.createText());
            contentPane.setVisible(true);
            /*int tmp = 0;
            for(int i = 0 ; i < ergebnisse.length ; i++) {
                tmp = tmp + Character.getNumericValue(ergebnisse[i].charAt(0));
                System.out.println(ergebnisse[i]);
            }
        System.out.println("Ergebnis des Fragebogens: " + tmp);*/
        });

        mntmExport.addActionListener(arg0 -> {
            try {
                new Export(ergebnisse, getPunktzahl() );
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    private void initWindow() {

        menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        mnDatei = new JMenu("Datei");
        menuBar.add(mnDatei);

        mntmExport = new JMenuItem("Export");
        mnDatei.add(mntmExport);

        mntmErgebnis = new JMenuItem("Ergebnis");
        mnDatei.add(mntmErgebnis);

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        frame.setContentPane(contentPane);
        frame.setTitle(index + " " + bogen.getItemAt(index).getTitle());
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{424, 0};
        gbl_contentPane.rowHeights = new int[]{318, 33, 0};
        gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl_contentPane);

        textPane = new JTextPane();
        textPane.setBackground(SystemColor.menu);
        textPane.setEditable(false);
        textPane.setText((bogen.getItemAt(index).getA1()));
        GridBagConstraints gbc_txtPane = new GridBagConstraints();
        gbc_txtPane.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtPane.insets = new Insets(0, 0, 5, 0);
        gbc_txtPane.gridx = 0;
        gbc_txtPane.gridy = 0;
        contentPane.add(textPane, gbc_txtPane);

        panel_bottom = new JPanel();
        GridBagConstraints gbc_panel_bottom = new GridBagConstraints();
        gbc_panel_bottom.anchor = GridBagConstraints.NORTH;
        gbc_panel_bottom.fill = GridBagConstraints.HORIZONTAL;
        gbc_panel_bottom.gridx = 0;
        gbc_panel_bottom.gridy = 1;
        contentPane.add(panel_bottom, gbc_panel_bottom);
        panel_bottom.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));


        textPane.setFont(font);

        contentPane.setVisible(true);
    }


    private int getPunktzahl() {
        int tmp = 0;
        for (int i = 0 ; i < ergebnisse.length ; i++) {
            tmp = tmp + Character.getNumericValue(ergebnisse[i].charAt(0));
        }
        return tmp;
    }

}
