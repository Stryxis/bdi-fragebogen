package UI;


import Questionaire.Bogen;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 */
 class FourItem {

    //Initialisierung der Attribute
    private static JFrame frame;
    private static Bogen bogen;
    private static String[] ergebnisse;
    private int index;
    private JPanel itemPane;
    private JRadioButton rdbtn_first;
    private JRadioButton rdbtn_second;
    private JRadioButton rdbtn_third;
    private JRadioButton rdbtn_fourth;
    private JTextPane txtpn_first;
    private JTextPane txtpn_second;
    private JTextPane txtpn_third;
    private JTextPane txtpn_fourth;
    private JButton btn_next;
    private Font font;

    /**
     *
     * @param frame übergebener Frame für die Anwendung
     * @param bogen übergebener Fragebogen
     * @param index momentane Stelle innerhalb des Fragebogens, welches Item momentan angezeigt werden soll
     * @param ergebnisse übergebener Ergebnisspeicher zur späteren Auswertung
     */
    FourItem(JFrame frame, Bogen bogen, int index, String[] ergebnisse) {

        font = new Font("Calibri", Font.PLAIN, 22);
        FourItem.frame = frame;
        FourItem.bogen = bogen;
        FourItem.ergebnisse = ergebnisse;
        this.index = index+1;
        initItemPanel();

    }

    /**
     *
     */
    private void initItemPanel() {

        //Übergebener Frame wird neu eingerichtet und gefüllt

        itemPane = new JPanel();
        itemPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        frame.setContentPane(itemPane);
        GridBagLayout gbl_itemPane = new GridBagLayout();
        gbl_itemPane.columnWidths = new int[]{0, 0, 0, 0, 0};
        gbl_itemPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
        gbl_itemPane.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        gbl_itemPane.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
        itemPane.setLayout(gbl_itemPane);
        frame.setTitle(index + " " + bogen.getItemAt(index).getTitle());

        //Platzhalter Panel für oben, dadurch kleiner Abstand oben zwischen Text und Titelleiste
        JPanel space_top = new JPanel();
        GridBagConstraints gbc_space_top = new GridBagConstraints();
        gbc_space_top.gridwidth = 4;
        gbc_space_top.insets = new Insets(0, 0, 5, 0);
        gbc_space_top.fill = GridBagConstraints.BOTH;
        gbc_space_top.gridx = 0;
        gbc_space_top.gridy = 0;
        itemPane.add(space_top, gbc_space_top);

        //Bereich für den ersten RadioButton mit dazugehörigem TextPane
        rdbtn_first = new JRadioButton("");
        initRButton(rdbtn_first, 1);

        txtpn_first = new JTextPane();
        initTPane(txtpn_first, 1);


        //Bereich für den zweiten RadioButton mit dazugehörigem TextPane
        rdbtn_second = new JRadioButton("");
        initRButton(rdbtn_second, 2);


        txtpn_second = new JTextPane();
        initTPane(txtpn_second, 2);


        //Bereich für den dritten RadioButton mit dazugehörigem TextPane
        rdbtn_third = new JRadioButton("");
        initRButton(rdbtn_third, 3);


        txtpn_third = new JTextPane();
        initTPane(txtpn_third, 3);


        //Bereich für den vierten RadioButton mit dazugehörigem TextPane
        rdbtn_fourth = new JRadioButton("");
        initRButton(rdbtn_fourth, 4);


        txtpn_fourth = new JTextPane();
        initTPane(txtpn_fourth,4);


        //Platzhalter Panel für links, dadurch kleiner Abstand am linken Rand
        JPanel space_left = new JPanel();
        GridBagConstraints gbc_space_left = new GridBagConstraints();
        gbc_space_left.gridheight = 4;
        gbc_space_left.insets = new Insets(0, 0, 5, 5);
        gbc_space_left.fill = GridBagConstraints.BOTH;
        gbc_space_left.gridx = 0;
        gbc_space_left.gridy = 1;
        itemPane.add(space_left, gbc_space_left);

        //Platzhalter Panel für rechts, dadurch kleiner Abstand am rechten Rand
        JPanel space_right = new JPanel();
        GridBagConstraints gbc_space_right = new GridBagConstraints();
        gbc_space_right.insets = new Insets(0, 0, 5, 0);
        gbc_space_right.gridheight = 4;
        gbc_space_right.fill = GridBagConstraints.BOTH;
        gbc_space_right.gridx = 3;
        gbc_space_right.gridy = 1;
        itemPane.add(space_right, gbc_space_right);

        //Platzhalter Panel für den Boden, dadurch kleiner Abstand zwischen Button und Frame
        JPanel space_bottom = new JPanel();
        GridBagConstraints gbc_space_bottom = new GridBagConstraints();
        gbc_space_bottom.gridwidth = 4;
        gbc_space_bottom.anchor = GridBagConstraints.NORTH;
        gbc_space_bottom.insets = new Insets(0, 0, 5, 0);
        gbc_space_bottom.fill = GridBagConstraints.HORIZONTAL;
        gbc_space_bottom.gridx = 0;
        gbc_space_bottom.gridy = 5;
        itemPane.add(space_bottom, gbc_space_bottom);

        //Abteil für den JButton zum weiter kommen
        btn_next = new JButton("Weiter");
        btn_next.setEnabled(false);

        GridBagConstraints gbc_btn_next = new GridBagConstraints();
        gbc_btn_next.gridwidth = 3;
        gbc_btn_next.insets = new Insets(0, 0, 0, 5);
        gbc_btn_next.gridx = 0;
        gbc_btn_next.gridy = 6;
        itemPane.add(btn_next, gbc_btn_next);

        //Font anwenden
        txtpn_first.setFont(font);
        txtpn_second.setFont(font);
        txtpn_third.setFont(font);
        txtpn_fourth.setFont(font);

        itemPane.setVisible(true);

        addAction();

    }

    /**
     * Auslagerung der Initialisierung der gesamten ActionListener.
     */
    private void addAction() {
        /*
         * ActionListener für die RadioButtons. Hier soll jeweils nur ein Button angewählt werden können. Wenn ein anderen angewählt wird
         * werden die anderen deaktiviert. Zudem soll der "Weiter"-Button erst funktionsfähig sein, wenn irgendein RadioButton aktiviert wurde.
         */
        rdbtn_first.addActionListener(arg0 -> {
            selektion(1);
        });

        rdbtn_second.addActionListener(arg0 -> {
            selektion(2);
        });

        rdbtn_third.addActionListener(arg0 -> {
            selektion(3);
        });

        rdbtn_fourth.addActionListener(arg0 -> {
            selektion(4);
        });
        /*
         * ActionListener für den "Weiter"-Button. Die, dem RadioButton zugeordnete, Antwort wird in das Ergebnis-Array gespeichert.
         * Das aktuelle Panel wird versteckt, dafür wird ein neues Panel für das nächste Item erzeugt.
         * Je nachdem wo man sich im Bogen befindet, wird ein passendes Item erzeugt.
         */
        btn_next.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER) {
                    weiter();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }

        });
        btn_next.addActionListener(arg0 -> {
            weiter();
        });

        txtpn_first.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(1);
            }

            @Override
            public void focusLost(FocusEvent e) {
            }
        });

        txtpn_second.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(2);
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });

        txtpn_third.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(3);
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });

        txtpn_fourth.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selektion(4);
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });
    }

    private void initRButton(JRadioButton jrb, int nummer) {
        GridBagConstraints gbc_rdbtn = new GridBagConstraints();
        gbc_rdbtn.insets = new Insets(0, 0, 5, 5);
        gbc_rdbtn.gridx = 1;
        gbc_rdbtn.gridy = nummer;
        itemPane.add(jrb, gbc_rdbtn);
    }

    private void initTPane(JTextPane jtp, int nummer) {
        jtp.setBackground(SystemColor.menu);

        if (nummer == 1) {
            jtp.setText(bogen.getItemAt(index).getA1());
        } else {
            if (nummer == 2) {
                jtp.setText(bogen.getItemAt(index).getA2());
            } else {
                if(nummer == 3) {
                    jtp.setText(bogen.getItemAt(index).getA3());
                } else {
                    jtp.setText(bogen.getItemAt(index).getA4());
                }
            }
        }

        GridBagConstraints gbc_txtpn = new GridBagConstraints();
        gbc_txtpn.insets = new Insets(0, 0, 5, 5);
        gbc_txtpn.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtpn.gridx = 2;
        gbc_txtpn.gridy = nummer;
        centerText(jtp);
        itemPane.add(jtp, gbc_txtpn);
    }

    private void weiter() {
        if (rdbtn_first.isSelected()) {
            ergebnisse[index-1] = bogen.getItemAt(index).getA1();
        } else {
            if(rdbtn_second.isSelected()) {
                ergebnisse[index-1] = bogen.getItemAt(index).getA2();
            } else {
                if(rdbtn_third.isSelected()) {
                    ergebnisse[index-1] = bogen.getItemAt(index).getA3();
                } else {
                    ergebnisse[index-1] = bogen.getItemAt(index).getA4();
                }
            }
        }
        itemPane.setVisible(false);
        if(index == 15 || index == 17) {
            new SevenItem(frame,bogen,index, ergebnisse);
        } else {
            if(index == 21) {
                new Ende(frame, bogen, index, ergebnisse);
            } else {
                new FourItem(frame, bogen, index, ergebnisse);
            }
        }
    }

    private void selektion(int nummer) {
        if(nummer == 1) {
            if(!rdbtn_first.isSelected() && !rdbtn_second.isSelected() && !rdbtn_third.isSelected() && !rdbtn_fourth.isSelected()) {
                rdbtn_first.setSelected(true);
            } else {
                rdbtn_first.setSelected(true);
                rdbtn_second.setSelected(false);
                rdbtn_third.setSelected(false);
                rdbtn_fourth.setSelected(false);
            }
        } else {
            if(nummer == 2) {
                if(!rdbtn_first.isSelected() && !rdbtn_second.isSelected() && !rdbtn_third.isSelected() && !rdbtn_fourth.isSelected()) {
                    rdbtn_second.setSelected(true);
                } else {
                    rdbtn_first.setSelected(false);
                    rdbtn_second.setSelected(true);
                    rdbtn_third.setSelected(false);
                    rdbtn_fourth.setSelected(false);
                }
            } else {
              if(nummer == 3) {
                  if(!rdbtn_first.isSelected() && !rdbtn_second.isSelected() && !rdbtn_third.isSelected() && !rdbtn_fourth.isSelected()) {
                      rdbtn_third.setSelected(true);
                  } else {
                      rdbtn_first.setSelected(false);
                      rdbtn_second.setSelected(false);
                      rdbtn_third.setSelected(true);
                      rdbtn_fourth.setSelected(false);
                  }
              } else {
                  if(!rdbtn_first.isSelected() && !rdbtn_second.isSelected() && !rdbtn_third.isSelected() && !rdbtn_fourth.isSelected()) {
                      rdbtn_fourth.setSelected(true);
                  } else {
                      rdbtn_first.setSelected(false);
                      rdbtn_second.setSelected(false);
                      rdbtn_third.setSelected(false);
                      rdbtn_fourth.setSelected(true);
                  }
              }
            }
        }
        btn_next.setEnabled(true);
        btn_next.grabFocus();
    }

    /**
     * Der Text in den TextPanes soll Horizontal formatiert werden, sodass er linksbündig ist.
     * @param textPane ist das zu formatierende JTextPane.
     */
    private void centerText(JTextPane textPane) {
        StyledDocument doc = textPane.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_LEFT);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }

}
