package UI;

import Questionaire.Bogen;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;

public class Chiffre {

    private static JFrame frame;
    private static Bogen bogen;
    private JPanel contentPane;
    private JTextField txtFName, txtFDate;
    private JButton btnStart;
    private static String[] ergebnisse;
    private Font font;
    public static char name;
    private char[] date;
    public static String datum;


    public Chiffre(JFrame frame, Bogen bogen, String[] ergebnisse) {

        font = new Font("Calibri", Font.PLAIN, 22);

        this.frame = frame;
        this.bogen = bogen;
        this.ergebnisse = ergebnisse;

        datum = "";

        initWindow();
        addAction();


    }

    private void addAction() {


        btnStart.addActionListener(arg0 -> {
            contentPane.setVisible(false);

            name = txtFName.getText().toUpperCase().charAt(0);

            date = txtFDate.getText().toCharArray();
            for (int i = 0; i < date.length ; i++) {
                if (date[i] != '.' && i != 6 && i != 7) {
                    datum = datum + date[i];
                }
            }

            new Deckblatt(frame, bogen, 0, ergebnisse);
        });
    }

    private void initWindow() {
        frame.setTitle("Dateneingabe");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 550, 300);
        frame.setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        frame.setContentPane(contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0};
        gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl_contentPane);

        JPanel panel_1 = new JPanel();
        GridBagConstraints gbc_panel_1 = new GridBagConstraints();
        gbc_panel_1.gridwidth = 3;
        gbc_panel_1.insets = new Insets(0, 0, 5, 0);
        gbc_panel_1.fill = GridBagConstraints.BOTH;
        gbc_panel_1.gridx = 0;
        gbc_panel_1.gridy = 0;
        contentPane.add(panel_1, gbc_panel_1);

        JPanel panel_3 = new JPanel();
        GridBagConstraints gbc_panel_3 = new GridBagConstraints();
        gbc_panel_3.gridheight = 7;
        gbc_panel_3.insets = new Insets(0, 0, 5, 0);
        gbc_panel_3.fill = GridBagConstraints.BOTH;
        gbc_panel_3.gridx = 2;
        gbc_panel_3.gridy = 1;
        contentPane.add(panel_3, gbc_panel_3);

        JPanel panel = new JPanel();
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.anchor = GridBagConstraints.WEST;
        gbc_panel.gridheight = 7;
        gbc_panel.insets = new Insets(0, 0, 5, 5);
        gbc_panel.fill = GridBagConstraints.VERTICAL;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 1;
        contentPane.add(panel, gbc_panel);

        JTextPane insertName = new JTextPane();
        insertName.setBackground(SystemColor.menu);
        insertName.setText("Bitte geben Sie Ihren Nachnamen ein:");
        insertName.setEditable(false);
        GridBagConstraints gbc_txtpnBitteGebenSie = new GridBagConstraints();
        gbc_txtpnBitteGebenSie.insets = new Insets(0, 0, 5, 5);
        gbc_txtpnBitteGebenSie.gridx = 1;
        gbc_txtpnBitteGebenSie.gridy = 2;
        centerText(insertName);
        contentPane.add(insertName, gbc_txtpnBitteGebenSie);


        txtFName = new JTextField();
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.insets = new Insets(0, 0, 5, 5);
        gbc_textField.gridx = 1;
        gbc_textField.gridy = 3;
        contentPane.add(txtFName, gbc_textField);
        txtFName.setColumns(10);

        JTextPane insertDate = new JTextPane();
        insertDate.setBackground(SystemColor.menu);
        insertDate.setText("Bitte geben Sie Ihr Geburtsdatum ein (dd.mm.yyyy):");
        insertDate.setEditable(false);
        GridBagConstraints gbc_txtpnBitteGebenSie_1 = new GridBagConstraints();
        gbc_txtpnBitteGebenSie_1.insets = new Insets(0, 0, 5, 5);
        gbc_txtpnBitteGebenSie_1.gridx = 1;
        gbc_txtpnBitteGebenSie_1.gridy = 4;
        centerText(insertDate);
        contentPane.add(insertDate, gbc_txtpnBitteGebenSie_1);


        txtFDate = new JTextField();
        GridBagConstraints gbc_textField_1 = new GridBagConstraints();
        gbc_textField_1.insets = new Insets(0, 0, 5, 5);
        gbc_textField_1.gridx = 1;
        gbc_textField_1.gridy = 5;
        contentPane.add(txtFDate, gbc_textField_1);
        txtFDate.setColumns(10);

        btnStart = new JButton("Start");
        GridBagConstraints gbc_btnStart = new GridBagConstraints();
        gbc_btnStart.insets = new Insets(0, 0, 5, 5);
        gbc_btnStart.gridx = 1;
        gbc_btnStart.gridy = 8;
        contentPane.add(btnStart, gbc_btnStart);

        JPanel panel_2 = new JPanel();
        GridBagConstraints gbc_panel_2 = new GridBagConstraints();
        gbc_panel_2.gridwidth = 3;
        gbc_panel_2.fill = GridBagConstraints.BOTH;
        gbc_panel_2.gridx = 0;
        gbc_panel_2.gridy = 8;
        contentPane.add(panel_2, gbc_panel_2);


        insertDate.setFont(font);
        insertName.setFont(font);
        txtFName.setFont(font);
        txtFDate.setFont(font);
    }

    private void centerText(JTextPane textPane) {
        StyledDocument doc = textPane.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_JUSTIFIED);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }
}
